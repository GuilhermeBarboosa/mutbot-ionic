export interface QuestionRequest {
    question: string;
    authorId: number;
}